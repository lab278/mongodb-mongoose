const mongoose = require('mongoose')
const Building = require('./models/Building')
const Room = require('./models/Room')
mongoose.connect('mongodb://localhost:27017/example')
async function main () {
  // Update
  // const room = await Room.findById('625a9e85e155e963a5549b67')
  // room.capacity = 20
  // room.save()
  // console.log(room)
  const room = await Room.findOne({ capacuty: { $gte: 100 } }).populate('building')
  console.log(room)
  console.log('--------------------')
  const rooms = await Room.find({ capacuty: { $gte: 100 } }).populate('building')
  console.log(rooms)
  const buildings = await Building.find({})
  console.log(JSON.stringify(buildings))
}

main().then(() => {
  console.log('Finish')
})
